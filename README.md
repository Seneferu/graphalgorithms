# Graph Algorithms

Implementation of some algorithm taught in the class *Introduction to Graph Algorithms*.
Feel free to send pull requests ;)

### Implemented Algorithm

- BFS
- DFS
- Finding blocks and articulation points
- Finding strongly connected components
- LexBFS
- Layering partition

### Project
The project is a C# console application created in Visual Studio 2015.
