﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphAlgorithms
{
    partial class DynamicTree
    {
        List<List<int>> vertexList;
        List<int> parentIds;

        public DynamicTree()
        {
            vertexList = new List<List<int>>();
            parentIds = new List<int>();

            // Create a new vertex and make it the root of the tree.
            RootId = AddVertex();
        }

        public DynamicTree(int capacity)
        {
            vertexList = new List<List<int>>(capacity);
            parentIds = new List<int>(capacity);
     
            // Create a new vertex and make it the root of the tree.
            RootId = AddVertex();
        }

        public int RootId { get; protected set; }

        public int Size
        {
            get
            {
                return vertexList.Count;
            }
        }

        /// <summary>
        /// Returns the ids of the neighbours of this vertex.
        /// </summary>
        public int[] this[int vId]
        {
            get
            {
                return vertexList[vId].ToArray();
            }
        }

        /// <summary>
        /// Adds a vertex without any connection to the forest.
        /// </summary>
        /// <returns>
        /// The id of the new vertex.
        /// </returns>
        private int AddVertex()
        {
            int newId = Size;
            vertexList.Add(new List<int>());
            parentIds.Add(-1);

            return newId;
        }

        /// <summary>
        /// Adds a new vertex to the forest and connects it with the given parent.
        /// Returns the id of the new vertex.
        /// </summary>
        /// <param name="parentId">
        /// Id of the parent vertex.
        /// </param>
        public int AddVertex(int parentId)
        {
            if (parentId < 0 || parentId >= Size)
            {
                throw new ArgumentOutOfRangeException();
            }

            int newId = AddVertex();

            vertexList[parentId].Add(newId);
            vertexList[newId].Add(parentId);

            parentIds[newId] = parentId;

            return newId;
        }

        /// <summary>
        /// Makes a vertex the root of the tree.
        /// </summary>
        public void MakeRoot(int vId)
        {
            Queue<int> idQ = new Queue<int>(Size);

            idQ.Enqueue(vId);
            parentIds[vId] = -1;
            RootId = vId;

            while (idQ.Count > 0)
            {
                int id = idQ.Dequeue();
                List<int> neighs = vertexList[id];

                foreach (int nId in neighs)
                {
                    if (nId == parentIds[id]) continue;

                    parentIds[nId] = id;
                    idQ.Enqueue(nId);
                }
            }
        }

        public int GetParent(int vId)
        {
            return parentIds[vId];
        }


    }
}
