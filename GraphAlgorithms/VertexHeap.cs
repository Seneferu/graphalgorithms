﻿
namespace GraphAlgorithms
{
    class VertexHeap
    {
        private int[] vertexPosition;
        private int[] weights;
        private int[] heap;

        public VertexHeap(int[] weights)
        {
            this.weights = weights;
            Size = weights.Length;

            heap = new int[Size];
            vertexPosition = new int[Size];

            for (int i = 0; i < Size; i++)
            {
                heap[i] = i;
                vertexPosition[i] = i;
            }

            BuildHeap();
        }

        public int Size { get; protected set; }

        public int ExtractMin()
        {
            int minId = heap[0];
            int lastId = heap[Size - 1];

            vertexPosition[minId] = -1;
            vertexPosition[lastId] = 0;

            heap[0] = lastId;
            Size--;

            Heapify(0);

            return minId;
        }

        public void Update(int vId)
        {
            int vInd = vertexPosition[vId];
            int vWei = weights[vId];

            while (vInd > 0)
            {
                int pInd = Parent(vInd);
                int pId = heap[pInd];
                int pWei = weights[pId];

                if (pWei <= vWei)
                {
                    break;
                }

                heap[pInd] = vId;
                heap[vInd] = pId;

                vertexPosition[vId] = pInd;
                vertexPosition[pId] = vInd;

                vInd = pInd;
            }

            Heapify(vInd);

        }

        private int Parent(int ind)
        {
            return (ind - 1) / 2;
        }

        private int Left(int ind)
        {
            return 2 * ind + 1;
        }

        private int Right(int ind)
        {
            return 2 * ind + 2;
        }

        private void BuildHeap()
        {
            for (int i = Size / 2 - 1; i >= 0; i--)
            {
                Heapify(i);
            }
        }

        private void Heapify(int ind)
        {
            while (true)
            {
                int l = Left(ind);
                int r = Right(ind);

                int vId = heap[ind];
                int lId = l < Size ? heap[l] : -1;
                int rId = r < Size ? heap[r] : -1;

                if (lId < 0) return;

                int sInd = ind;
                int sId = vId;

                if (weights[lId] < weights[sId])
                {
                    sInd = l;
                    sId = lId;
                }

                if (rId >= 0 && weights[rId] < weights[sId])
                {
                    sInd = r;
                    sId = rId;
                }

                if (sInd == ind) return;

                heap[ind] = sId;
                heap[sInd] = vId;

                vertexPosition[vId] = sInd;
                vertexPosition[sId] = ind;

                ind = sInd;
            }
        }

    }
}
