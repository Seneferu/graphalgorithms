﻿using System;
using System.Collections.Generic;

namespace GraphAlgorithms
{
    public struct BlockInfo
    {
        public int[] ArticulationPoints;
        public int[][] Blocks;
    }

    partial class Graph
    {
        /// <summary>
        /// Stores the "global" data for computing the blocks of a graph.
        /// </summary>
        private class BlockSearchInfo
        {
            public bool[] visited;

            public int[] parent;
            public int[] neighIndex;

            public int[] depth;
            public int[] low;

            private Stack<int> stack;
            private List<int> artPointList;
            private List<List<int>> blockList;

            public BlockSearchInfo(int vertices)
            {
                stack = new Stack<int>();

                visited = new bool[vertices];

                parent = new int[vertices];
                neighIndex = new int[vertices];

                depth = new int[vertices];
                low = new int[vertices];

                blockList = new List<List<int>>();
                artPointList = new List<int>();

                for (int i = 0; i < vertices; i++)
                {
                    visited[i] = false;

                    parent[i] = -1;
                    neighIndex[i] = 0;

                    depth[i] = -1;
                    low[i] = -1;
                }
            }

            public void Push(int vId)
            {
                stack.Push(vId);
            }

            public void NewBlock(int rootId)
            {
                List<int> block = new List<int>();

                for (int lastId = -1; lastId != rootId;)
                {
                    lastId = stack.Pop();
                    block.Add(lastId);
                }

                // Root of the block remains on the stack becaus it can be the root of multiple blocks.
                if (parent[rootId] >= 0)
                {
                    block.Add(parent[rootId]);
                }

                blockList.Add(block);
            }

            public void AddArtPoint(int vId)
            {
                artPointList.Add(vId);
            }

            public BlockInfo ToBlockInfo()
            {
                BlockInfo bi = new BlockInfo();

                bi.ArticulationPoints = artPointList.ToArray();
                bi.Blocks = new int[blockList.Count][];

                for (int i = 0; i < blockList.Count; i++)
                {
                    bi.Blocks[i] = blockList[i].ToArray();
                }

                return bi;
            }
        }

        /// <summary>
        /// Determines all blocks and articulation points of the graph.
        /// Will not work properly if the graph is not undirected.
        /// </summary>
        public BlockInfo GetBlocks()
        {
            BlockSearchInfo bsi = new BlockSearchInfo(Vertices);

            for (int vId = 0; vId < Vertices; vId++)
            {
                if (bsi.visited[vId]) continue;
                GetBlocks(vId, bsi);
            }

            return bsi.ToBlockInfo();
        }

        private void GetBlocks(int rootId, BlockSearchInfo bsi)
        {
            // Is isolated vertex?
            if (this[rootId].Length == 0)
            {
                bsi.Push(rootId);
                bsi.NewBlock(rootId);
                return;
            }

            Stack<int> dfsStack = new Stack<int>();

            dfsStack.Push(rootId);
            bsi.depth[rootId] = 0;

            // Counts the children of the root in the DFS-tree.
            int childCount = 0;

            while (dfsStack.Count > 0)
            {
                int vId = dfsStack.Peek();
                int nInd = bsi.neighIndex[vId];

                if (nInd == 0)
                {
                    bsi.visited[vId] = true;

                    // *** Pre-order for vId ***
                    bsi.low[vId] = bsi.depth[vId];
                    bsi.Push(vId);

                    if (bsi.parent[vId] == rootId)
                    {
                        childCount++;
                    }
                }

                if (nInd < this[vId].Length)
                {
                    int neighId = this[vId][nInd];

                    if (!bsi.visited[neighId])
                    {
                        dfsStack.Push(neighId);
                        bsi.parent[neighId] = vId;
                        bsi.depth[neighId] = bsi.depth[vId] + 1;
                    }

                    bsi.neighIndex[vId]++;
                }
                else
                {
                    // All neighbours checked, backtrack.
                    dfsStack.Pop();

                    // *** Post-order for vId ***

                    if (vId == rootId)
                    {
                        if (childCount > 1)
                        {
                            bsi.AddArtPoint(vId);
                        }
                        continue;
                    }

                    // Update low(v) and check if v is articulation point.
                    bool isArtPoint = false;

                    foreach (int neiId in this[vId])
                    {
                        bsi.low[vId] = Math.Min(bsi.low[vId], bsi.low[neiId]);
                        isArtPoint = isArtPoint || bsi.low[neiId] >= bsi.depth[vId];
                    }

                    if (isArtPoint)
                    {
                        bsi.AddArtPoint(vId);
                    }

                    // Check if parent is root of a block.
                    int parId = bsi.parent[vId];

                    if (parId == rootId || bsi.low[vId] >= bsi.depth[parId])
                    {
                        // Parent is root of block.
                        bsi.NewBlock(vId);
                    }
                }
            }
        }

    }
}
