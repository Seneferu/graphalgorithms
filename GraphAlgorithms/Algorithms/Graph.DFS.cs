﻿using System.Collections.Generic;

namespace GraphAlgorithms
{
    public struct DfsResult
    {
        public int StartId { get; set; }
        public int[] ParentIds { get; set; }
        public int[] PreOrder { get; set; }
        public int[] PostOrder { get; set; }
    }

    partial class Graph
    {
        /// <summary>
        /// Runs a DFS.
        /// </summary>
        /// <param name="startId">
        /// The ID of the start vertex.
        /// </param>
        public DfsResult Dfs(int startId)
        {
            Stack<int> stack = new Stack<int>();

            List<int> preOrder = new List<int>(Vertices);
            List<int> postOrder = new List<int>(Vertices);

            bool[] visited = new bool[Vertices];
            int[] neighIndex = new int[Vertices];
            int[] parIds = new int[Vertices];

            for (int vId = 0; vId < Vertices; vId++)
            {
                visited[vId] = false;
                neighIndex[vId] = 0;
                parIds[vId] = -1;
            }

            stack.Push(startId);

            while (stack.Count > 0)
            {
                int vId = stack.Peek();
                int nInd = neighIndex[vId];

                if (nInd == 0)
                {
                    visited[vId] = true;

                    // *** Pre-order for vId ***
                    preOrder.Add(vId);
                }

                if (nInd < this[vId].Length)
                {
                    int neighId = this[vId][nInd];

                    if (!visited[neighId])
                    {
                        stack.Push(neighId);
                        parIds[neighId] = vId;
                    }

                    neighIndex[vId]++;
                }
                else
                {
                    // All neighbours checked, backtrack.
                    stack.Pop();

                    // *** Post-order for vId ***
                    postOrder.Add(vId);
                }
            }

            return new DfsResult()
            {
                StartId = startId,
                PreOrder = preOrder.ToArray(),
                PostOrder = postOrder.ToArray(),
                ParentIds = parIds
            };
        }

    }
}
