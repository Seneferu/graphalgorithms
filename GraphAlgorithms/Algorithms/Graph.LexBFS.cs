﻿
namespace GraphAlgorithms
{
    partial class Graph
    {

        private struct VertexInfo
        {
            public int position;
            public int groupId;
            public bool visited;
        }

        private struct Group
        {
            public int first;
            public int last;
            public int level;
            public int prev;
        }

        public int[] LexBFS()
        {
            return LexBFS(null);
        }

        public int[] LexBFS(int[] preOrder)
        {
            int[][] orderedVertices = graph;

            if (preOrder == null)
            {
                preOrder = new int[Vertices];
                for (int i = 0; i < Vertices; i++)
                {
                    preOrder[i] = Vertices - i - 1;
                }
            }
            else // (preOrder != null)
            {
                orderedVertices = new int[Vertices][];
                int[] neighIndex = new int[Vertices];

                for (int i = 0; i < Vertices; i++)
                {
                    orderedVertices[i] = new int[this[i].Length];
                    neighIndex[i] = 0;
                }

                for (int i = 0; i < Vertices; i++)
                {
                    int vId = preOrder[i];
                    int[] neighs = this[i];

                    for (long j = 0; j < neighs.Length; j++)
                    {
                        int jId = neighs[j];

                        int[] jNeighs = orderedVertices[jId];
                        jNeighs[neighIndex[jId]] = vId;

                        neighIndex[jId]++;
                    }
                }
            }

            int[] order = new int[Vertices];
            VertexInfo[] vInfo = new VertexInfo[Vertices];
            Group[] groups = new Group[Vertices];

            int grpCounter = 0;

            groups[0].first = 0;
            groups[0].last = Vertices - 1;
            groups[0].level = -1;
            groups[0].prev = -1;

            // "Copy" vertices into order
            for (int i = 0; i < Vertices; i++)
            {
                int vId = preOrder[Vertices - i - 1];
                order[i] = vId;
                vInfo[vId].position = i;
            }

            // Run LexBFS
            for (int i = 0; i < Vertices; i++)
            {
                int vId = order[i];
                vInfo[vId].visited = true;

                // Remove vertex from group.
                int vGrpId = vInfo[vId].groupId;
                groups[vGrpId].first++;

                int[] neighs = orderedVertices[vId];

                for (int j = 0; j < neighs.Length; j++)
                {
                    int nId = neighs[j];

                    if (vInfo[nId].visited) continue;

                    // Determine group and its first vertex.
                    int grId = vInfo[nId].groupId;

                    int firstPos = groups[grId].first;
                    int firstId = order[firstPos];

                    int nPos = vInfo[nId].position;

                    // Switch position with head of group
                    if (firstId != nId)
                    {
                        vInfo[firstId].position = nPos;
                        vInfo[nId].position = firstPos;

                        order[nPos] = firstId;
                        order[firstPos] = nId;

                        nPos = firstPos;
                    }

                    // Vertex is first in group now.

                    if (groups[grId].first != groups[grId].last)
                    {
                        // Make group smaller
                        groups[grId].first++;

                        int prevGrpId = groups[grId].prev;

                        if (prevGrpId < 0 || groups[prevGrpId].level < i)
                        {
                            // Make new group.
                            grpCounter++;

                            Group newGrp = new Group();

                            newGrp.first = nPos;
                            newGrp.level = i;
                            newGrp.prev = prevGrpId;

                            groups[grpCounter] = newGrp;
                            groups[grId].prev = grpCounter;

                            prevGrpId = grpCounter;
                        }

                        // Add vertex to previous group.
                        vInfo[nId].groupId = prevGrpId;
                        groups[prevGrpId].last = nPos;
                    }
                }
            }

            return order;
        }

    }
}
