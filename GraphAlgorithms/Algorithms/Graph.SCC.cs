﻿using System;
using System.Collections.Generic;

namespace GraphAlgorithms
{
    partial class Graph
    {
        private class SccSearchInfo
        {
            private int indexCounter;

            public int[] index;
            public int[] low;
            public int[] neighIndex;
            public bool[] onStack;

            private Stack<int> stack;
            private List<List<int>> sccList;

            public SccSearchInfo(int vertices)
            {
                stack = new Stack<int>();
                sccList = new List<List<int>>();

                indexCounter = 0;
                index = new int[vertices];
                low = new int[vertices];
                onStack = new bool[vertices];
                neighIndex = new int[vertices];

                for (int i = 0; i < vertices; i++)
                {
                    index[i] = -1;
                    low[i] = -1;
                    neighIndex[i] = 0;
                    onStack[i] = false;
                }

            }

            public void SetIndex(int vId)
            {
                index[vId] = indexCounter;
                indexCounter++;
            }

            public void Push(int vId)
            {
                stack.Push(vId);
            }

            public void NewScc(int rootId)
            {
                List<int> scc = new List<int>();
                int vId;

                do
                {
                    vId = stack.Pop();
                    onStack[vId] = false;
                    scc.Add(vId);
                }
                while (rootId != vId);

                sccList.Add(scc);
            }

            public int[][] GetComponents()
            {
                int[][] components = new int[sccList.Count][];

                for (int i = 0; i < sccList.Count; i++)
                {
                    components[i] = sccList[i].ToArray();
                }

                return components;
            }

        }

        public int[][] GetStronglyConnectedComponents()
        {
            SccSearchInfo ssi = new SccSearchInfo(Vertices);

            for (int vId = 0; vId < Vertices; vId++)
            {
                if (ssi.index[vId] != -1) continue;
                GetStronglyConnectedComponents(vId, ssi);
            }

            return ssi.GetComponents();
        }

        private void GetStronglyConnectedComponents(int startId, SccSearchInfo ssi)
        {
            Stack<int> dfsStack = new Stack<int>();

            dfsStack.Push(startId);

            while (dfsStack.Count > 0)
            {
                int vId = dfsStack.Peek();
                int nInd = ssi.neighIndex[vId];

                if (nInd == 0)
                {
                    // *** Pre-order for vId ***
                    ssi.SetIndex(vId);
                    ssi.low[vId] = ssi.index[vId];

                    ssi.Push(vId);
                    ssi.onStack[vId] = true;
                }

                if (nInd < this[vId].Length)
                {
                    int neighId = this[vId][nInd];

                    if (ssi.index[neighId] == -1)
                    {
                        dfsStack.Push(neighId);
                    }

                    ssi.neighIndex[vId]++;
                }
                else
                {
                    // All neighbours checked, backtrack.
                    dfsStack.Pop();

                    // *** Post-order for vId ***

                    // Update lowpoint
                    foreach (int neighId in this[vId])
                    {
                        if (!ssi.onStack[neighId]) continue;

                        if (ssi.index[neighId] > ssi.index[vId])
                        {
                            // Descendant
                            ssi.low[vId] = Math.Min(ssi.low[vId], ssi.low[neighId]);
                        }
                        else
                        {
                            // Cross or back edge
                            ssi.low[vId] = Math.Min(ssi.low[vId], ssi.index[neighId]);
                        }
                    }

                    // If v is a root node, pop the stack and generate an SCC.
                    if (ssi.low[vId] == ssi.index[vId])
                    {
                        // Start a new strongly connected component.
                        ssi.NewScc(vId);
                    }
                }
            }
        }

    }
}
